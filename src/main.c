#include "cnv.h"

int main()
{
    int ret = 0;
    int attrs[3] = {34, 5987662, 123};
    int attr_count = 3;
    
    BZR_obj foo_a = 0;
    
    ret = ch_vis(foo_a, attr_count, attrs);
    if(ret)
        cnv_pll(foo_a, attr_count, attrs);
    
    return ret;
}
