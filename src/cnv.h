#ifdef __cplusplus
extern "C" {
#endif

#define CAN_DO 6
#define CAN_NOT 7
#define CAN_TYPE 98
#define CAN_TYPE_2 99
#define CAN_TYPE_3 199
    
typedef	struct	{

	unsigned 
	int		wr_type : 32,
			wr_size : 32;

} BZR_req;

typedef	int *	BZR_obj;

extern BZR_req * cnv_pll(BZR_obj cnv, int nr_pnts, int *pnts);
extern int ch_vis(BZR_obj dest, int nr_attributes, int *attributes);

#ifdef __cplusplus
}
#endif
