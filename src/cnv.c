#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include "stdio.h"


#include "cnv.h"

#define USE_SOCKETS 1

// HERE's MY CODE

struct BZR_globals {
	int		wm_in;
	int		wm_out;
	int		inhibit_input;
	int		input_activity;
	int		autopilot;
};

struct BZR_globals Bzr = {
	-1,
	-1,
	0,
	0,
	0
};

void default_handler(void)
{
    return;
}


int readm(char *r_buff, int size)
{
	int		k, st;
	char	*r;
    int retries = 10;

	k = size;
	r = r_buff;
	while (k && retries > 0)  {
		
       /* try to read r bytes and see how many bytes we got  */
        if (USE_SOCKETS)
	       st = recv(Bzr.wm_in, r, k, 0);
        else
	       st = read(Bzr.wm_in, r, k);

		if (st <= 0)
			retries--;
		else {
			r += st;
			k -= st;
			retries = 10;
		}

	} /* end retry loop  */

	if (retries) return(0);

	fprintf(stderr, "Expected size %d, received %d\n", size, size - k);

	return -1;
}

BZR_req * cnv_pll(
	BZR_obj cnv,
	int nr_pnts,	  
	int *pnts
	)
{
	int				*r_buff, l_buff[128], size, n_ints;
	short			*coord;
	BZR_req *req;
	 
	n_ints = sizeof(BZR_req)/sizeof(int);
	n_ints++;	/* object handle */
	n_ints++;	/* nr or points */
	n_ints += nr_pnts;	/* one int per point */
	if (n_ints >= sizeof(l_buff)/sizeof(int))
		r_buff = (int *) malloc(n_ints * sizeof(int));
	else
		r_buff = l_buff;

	req			 = (BZR_req *) r_buff;
	req->wr_type = CAN_TYPE;
	n_ints = sizeof(*req)/sizeof(int);

	r_buff[n_ints++] = (int) cnv;
	r_buff[n_ints++] = nr_pnts;
	coord			 = (short *) &r_buff[n_ints];
	while (nr_pnts) {
		*coord++ = *pnts++;
		*coord++ = *pnts++;
		n_ints++;
		nr_pnts--;
	}

	size = n_ints * sizeof(int);
	req->wr_size = size - sizeof(*req);

    return req;
}

int ch_vis(
	BZR_obj	dest,
	int			nr_attributes,
	int			*attributes)
{
	int				*r_buff, l_buff[40], size, done, reply,
					n_ints, result = 0;
	unsigned int	list_sz;
	BZR_req *req;
	 
	if (nr_attributes < 1) return(0);	/* caller must pass at least one arg.*/
	list_sz = 2*nr_attributes*sizeof(int);
	/*
	**	First determine if we can use the local buffer or do
	**	we need to allocate a larger one.
	*/
	n_ints  = 1;	/* size of package */
	n_ints += sizeof(BZR_req)/sizeof(int);
	n_ints++;	/* destination handle */
	n_ints++;	/* # of attributes */
	n_ints += 2*nr_attributes;

	if (n_ints*sizeof(int) > sizeof(l_buff))
		r_buff = (int *) malloc(n_ints * sizeof(int));
	else
		r_buff = l_buff;

	/* Reserve first word for block size. */
	n_ints		 = 1;
	req			 = (BZR_req *) &r_buff[1];
	req->wr_type = CAN_TYPE_2;
	n_ints	    += sizeof(*req)/sizeof(int);

	r_buff[n_ints++] = (int) dest;
	r_buff[n_ints++] = nr_attributes;
	memcpy((char *) &r_buff[n_ints], (char *) attributes, list_sz);
	n_ints += 2*nr_attributes;

	size = (n_ints - 1) * sizeof(int); /* exclude packet size */
	req->wr_size = size - sizeof(*req);

	// TODO: local write here

	if (r_buff != l_buff)
		free((char *) r_buff);

	done = 0;
	while (!done) {
		readm((char *) &reply, (int ) 4);

		switch (reply) {
        case CAN_TYPE_3 :
				done = 1;
				readm((char *) &result, sizeof(int));
				if (result < 1) return(result);
				readm((char *) attributes, list_sz);
				break;
			default :
				default_handler();
				break;
		}
	}
	return(result);
}
